let collection = [];

// Write the queue functions below.
function print(){
	return collection;
}

function enqueue(element){
	     collection.push(element);
        return collection;
        
} 

function dequeue(element){
 	collection.shift(element);
        return collection;
} 

function front(){
	return collection[0];
} 

function size(){
	return collection.length;
}


function isEmpty(){
	if(collection.length == 0 ){
		return true
	}
	return false
}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};